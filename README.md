# Portfolio Webtechnologie 2

## Les 1 - version control with GIT

De eerste les was een kennismaking met Git, een handig en zeer populair systeem om aan versiebeheer te doen. 
Bij kleine zelfstandige projecten had ik er nog niet zo een nood aan, omdat je geen rekening moet houden met anderen en je zelf goed weet waar je mee bezig bent.
Maar doorheen de opleiding werd al snel duidelijk dat in team aan 1 project werken praktisch onmogelijk was zonder Git.

### Basis commando's

    __git clone__
    een repository van git naar je lokaal systeem kopiëren

    __git add *bestand.html*__
    een bestand klaarzetten om te committen

    __git commit -m'*hier een bericht over je commit*'__
    met dit commando commit je je bestanden

    __git push origin *branchnaam*__
    het enige wat je nog moet doen na een commit is het pushen naar je remote repository

    __git pull__
    wanneer iemand aanpassingen heeft gepushed naar de repository, kunnen anderen van je groep de bestanden binnenhalen met dit commando


### Branches

Branches zijn handig om verschillende versies van je project te beheren. Je project heeft altijd een master branch, dit is de hoofdbranch waar je mee begint wanneer je een repository aanmaakt. Wanneer je een nieuwe functionaliteit wil toevoegen kan het handig zijn om dit in een nieuwe branch te doen, en daar al je aanpassingen in te committen. Je kan deze branch vervolgens mergen met je master branch, maar het is aangeraden dat je dit pas doet wanneer de nieuwe functionaliteit zo goed als bugvrij is.

    __git branch *branchnaam*__
    Een nieuwe branch aanmaken

    __git checkout *branchnaam*__
    Switchen naar een branch

    __git merge *branchnaam*__
    Een branch samenvoegen met de master branch

## Les 2 - Mastering CSS Animations

