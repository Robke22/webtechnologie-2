$(document).ready(function(){
   
    // socket connection maken
	var socket = io.connect('http://localhost:3000');
		
	$("#username").on("keyup", function(){

    });
    
    $('#submit').on('click', function() {
        socket.emit('sendMessage', $("#message").val(), $("#username").val());
        $("#message").val("");
    });
    
    socket.on('update', function(update) {
        $('#messages').prepend('<li data-id="' + update._id + '">' + 
                              '<h2>' + update.user + '</h2>' +
                              '<p>' + update.message + '</p>' +
                              '</li>');
    });
    
    socket.on('loadmessages', function(messages) {
        $.each(messages, function(i, message){
             $('#messages').prepend('<li data-id="' + message._id + '">' + 
                              '<h2>' + message.user + '</h2>' +
                              '<p>' + message.message + '</p>' +
                              '</li>');
        });
       
    });
    
    
});