var express = require('express');
var router = express.Router();

/* GET Ask A Question page. */
router.get('/', function(req, res, next) {
  res.render('ask');
});

module.exports = router;
