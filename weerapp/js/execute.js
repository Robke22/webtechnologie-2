var latitude, longitude;

// verkrijgen van de huidige positie van de gebruiker via geolocation
navigator.geolocation.getCurrentPosition(success, error);


function success(position) {
  var pos = position.coords;
    latitude = pos.latitude;
    longitude = pos.longitude;
 
    // succesvol --> weerapp wordt gestart
  farmWeather = new weerapp();
  farmWeather.showweather();
};

function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
};