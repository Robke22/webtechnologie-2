var localweather = new Array();
var weerapp, farmWeather, url;


weerapp = function () {

    this.showweather = function () {

        if (localStorage.getItem('locweather') !== null) {
            // get from cache
            var stations = JSON.parse(localStorage.getItem('locweather'));
        }

        key = "9bd2eeaa65fe05956161336538dfd9ee"
        url = "https://api.forecast.io/forecast/" + key + "/" + latitude + "," + longitude + "?units=si";
        
            $.ajax({
              type: 'GET',
              dataType: 'jsonp',
              url: url,
              success: function(data){
                    var currentweather = data.currently;
                    var dailyweather = data.daily;

                    // opslaan in localStorage
                    localStorage.setItem("locweather", JSON.stringify(currentweather));

                  
                    // stop alle data in array localweather[]
                    localweather['samenvatting'] = currentweather.summary;
                    localweather['windsnelheid'] = currentweather.windSpeed * 3.6;
                    localweather['bewolking'] = currentweather.cloudCover;
                    localweather['temperatuur'] = currentweather.temperature;
                    localweather['icoon'] = currentweather.icon;
                    localweather['neerslag'] = currentweather.precipIntensity;
                    localweather['date'] = currentweather.time;
                    
                    // dag van de week berekenen
                    var currenttime = new Date(localweather['date']*1000);
                    var todaysday = currenttime.getDay();
                    
                    // minimum en maximum temperaturen in localweather[] stoppen
                    localweather['min-temp'] = dailyweather.data[todaysday].temperatureMin;
                    localweather['max-temp'] = dailyweather.data[todaysday].temperatureMax;

                    // print de data
                    $("#wind").html(Math.round(localweather['windsnelheid']) + " km/u");
                    $("#currentdeg").html(Math.round(localweather['temperatuur']) + " <span class='deg'>&deg;</span>");
                    $("#summary").html(localweather['samenvatting']);
                    $("#weathericon").css('background-image', 'url(images/' + localweather['icoon'] +'.png) ');
                    $("#rain").html(localweather['neerslag'] + ' mm');
                    $("#date").html(getTodaysDate(currenttime));
                    $("#minmax-deg").html(Math.round(localweather['min-temp']) + "&deg; / " + Math.round(localweather['max-temp']) + "&deg;");
                  
                    setBackgroundColor(localweather['icoon']);
                },
                error: function () {
                    
                }
            });
    };
};

    
// functie om de datum van vandaag te verkrijgen
function getTodaysDate(date) {
    var today = new Date(date);
    var day = today.getDate();
    var dayoftheweek = today.getDay();
    var month = today.getMonth()+1;
    var year = today.getFullYear();
    var months = new Array(12);
    var days = new Array(7);

    months[1] = "januari";
    months[2] = "februari";
    months[3] = "maart";
    months[4] = "april";
    months[5] = "mei";
    months[6] = "juni";
    months[7] = "juli";
    months[8] = "augustus";
    months[9] = "september";
    months[10] = "oktober";
    months[11] = "november";
    months[12] = "december";
    
    days[1] = "Maandag";
    days[2] = "Dinsdag";
    days[3] = "Woensdag";
    days[4] = "Donderdag";
    days[5] = "Vrijdag";
    days[6] = "Zaterdag";
    days[7] = "Zondag";
    
    return days[dayoftheweek] + ' ' + day + ' ' + months[month] + ' ' + year;

};


// achtergrondkleur veranderen naargelang dag,nacht of neerslag
function setBackgroundColor(weathercondition){
    if(weathercondition == "partly-cloudy-night" || weathercondition == "clear-night"){
        $("#wrapper").css('background-color', '#07121d');
        
    } else if(weathercondition == "rain" || weathercondition == "snow" || weathercondition == "fog" || weathercondition == "sleet"){
        $("#wrapper").css('background-color', '#889199');
        
    } else{
        $("#wrapper").css('background-color', '#2290a8');
    }
}










